const { truck } = require('../model/mongoMain/mongoConections.js');

const isOnLoad = async(driverId) => {
    const searchParams = {
        created_by:driverId,
        status:'OL',
        assigned_to:driverId
    }
    const list = await truck.find(searchParams);
    
    if(list.length > 0){
        throw new Error(`driver is ON LOAD now!`);
    }
    else{
        return true;
    }
}

module.exports = { isOnLoad };
