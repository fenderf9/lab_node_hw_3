const state = [
    `En route to Pick Up`, 
    `Arrived to Pick Up`, 
    `En route to delivery`, 
    `Arrived to delivery`
]

const changeState = async(now) => {
    const len = state.length;
    for(let i = 0; i < len - 1; i++){
        if (now == state[len -1]){
            return state[len -1];
        }
        if (now != state[i]){
            continue
        }
        else{
            return state[i + 1];
        }
    }
    throw new Error(`wrong string`)
}

module.exports = { changeState }