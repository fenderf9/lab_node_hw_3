const mongoose = require('mongoose');
const { userSchema } = require('../schemas/user.js');
const { truckSchema } = require('../schemas/truck.js');
const { loadSchema } = require('../schemas/load.js');

require('dotenv').config()
mongoose.connect(process.env.DB_CONNECTION, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
})

  const user = mongoose.model(`user`, userSchema);
  const truck = mongoose.model(`truck`, truckSchema);
  const load = mongoose.model(`load`, loadSchema);

  module.exports = { user, truck, load };