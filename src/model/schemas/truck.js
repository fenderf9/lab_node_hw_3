const mongoose = require('mongoose');

const truckSchema = new mongoose.Schema({
    created_by:String,

    assigned_to:{
        type: String,
        default: `null`,
    },

    type:{
        type: String,
        enum: [`SPRINTER`, `SMALL STRAIGHT`, `LARGE STRAIGHT`],
        required: true
    },

    status:{
        type: String,
        enum: [`OL`, `IS`],
        default: `IS`
    },

    created_date:String
})

module.exports = { truckSchema };