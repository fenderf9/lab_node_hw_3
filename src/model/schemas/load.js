const mongoose = require('mongoose');
const paginate = require('mongoose-paginate-v2');

const loadSchema = new mongoose.Schema({
    created_by:String,

    assigned_to:String,

    status:{
        type:String,
        enum:['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
        default:'NEW'
    },

    state:{
        type:String,
        enum:[
            'En route to Pick Up', 
            'Arrived to Pick Up', 
            'En route to delivery', 
            'Arrived to delivery',
            `null`
        ],
        defalut:`null`
    },

    name:{
        type:String,
        required:true 
    },

    payload:{
        type:Number,
        required:true
    },

    pickup_address:{
        type:String,
        required:true
    },

    delivery_address:{
        type:String,
        required:true
    },

    dimensions:{
        width:{
            type:Number,
            required:true
        },

        length:{
            type:Number,
            required:true
        },

        height:{
            type:Number,
            required:true
        },
    },

    logs:[{
        _id:false,
        
        message:String,

        time:String
    }],

    created_date:String
})

loadSchema.plugin(paginate);
module.exports = { loadSchema };