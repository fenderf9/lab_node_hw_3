const expres = require('express');
const app = expres();
const { authRouter } = require('./middleware/authMiddleware.js');
const { userRouter } = require('./middleware/userMiddleware.js');
const { truckRouter } = require('./middleware/truckMiddleware.js');
const { loadRouter } = require('./middleware/loadMiddleware.js');
require('dotenv').config()

app.use( expres.json() );

app.use(`/api/auth`, authRouter);
app.use(`/api/users`, userRouter);
app.use(`/api/trucks`, truckRouter);
app.use(`/api/loads`, loadRouter);

app.listen(process.env.PORT, () => {console.log('server is running')});