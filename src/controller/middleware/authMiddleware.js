const express = require('express');
const authRouter = express.Router();
const { handler } = require('../../utilites/errorHandler.js');
const { userValidator } = require('../../validator/userValidator.js');
const bcrypt = require('bcrypt');
const { user } = require('../../model/mongoMain/mongoConections.js');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const generator = require('generate-password');
require('dotenv').config()

authRouter.post(`/register`, async (req, res) => {
    const {email, password, role} = req.body;
    const validationResult = await userValidator(email, password, role);
    
    if(! validationResult.value){
       return handler.send400Error(res, validationResult.message);
    }

    if( !(email && password && role) ){
        return handler.send400Error(res, 'email/password/role are not defined');
    }

    try{
        const newUser = new user({
            email,
            password:await bcrypt.hash(password, 10),
            role,
            createdDate: new Date( Date.now() ),
        })
        await newUser.save();
    }
    catch(err){
        return handler.send400Error(res, err.message );
    }

    res.status(200).json({message:'Profile created successfully'})
    
})

authRouter.post('/login', async(req, res) => {
    const {email, password} = req.body;
    const validationResult = await userValidator(email, password);
    
    if(! validationResult.value){
       return handler.send400Error(res, validationResult.message);
    }

    if( !(email && password) ){
        return handler.send400Error(res, 'email/password are not defined');
    }

    try{
       const findUser = await user.findOne({ email });

        if(! findUser){
            return handler.send400Error(res, 'user not found');
        }

        if(! await bcrypt.compare(password, findUser.password)){
            return handler.send400Error(res, 'wrong password');
        }

        const payload = {
            email:findUser.email,
            role:findUser.role,
            _id:findUser._id,
        }
        
        const token = jwt.sign(payload, process.env.SECRET);

        return response(res, token)
    }
    catch(err){
        return handler.send400Error(res, err.message)
    }

    function response (res, token){
        return res.status(200).json({jwt_token:token})
    }
})

authRouter.post(`/forgot_password`, async(req, res) => {
    const email  = req.body.email;
    try{
        if(! email){
            throw new Error(`email is undefined`)
        }

        const validationResult = await userValidator(email);
    
        if(! validationResult.value){
            throw new Error(validationResult.message);
        }

        const password =  generator.generate({
            length:10,
            numbers:true
        })

        const transporter =  nodemailer.createTransport({
            host: process.env.HOST,
            port: 2525,
            auth: {
                user: process.env.USER,
                pass: process.env.PASS
            }
          });
          
          const mailOptions = {
            from: 'fenderf9@gmail.com',
            to: email,
            subject: 'Sending new Password using Node.js',
            text: `this is you new password: ${password}`
          };

          await transporter.sendMail(mailOptions);

          const update = {password: await bcrypt.hash(password, 10)}
              
          await user.updateOne({email:email},update);
    }
    catch(err){
        return handler.send400Error(res, err.message);
    }

    return res.status(200).json({message:'New password sent to your email address'})
    
})

module.exports = { authRouter };