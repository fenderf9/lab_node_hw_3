const express = require('express');
const userRouter = express.Router();
const { tokenValidation } = require('../../validator/tokenValidation.js');
const { handler } = require('../../utilites/errorHandler.js');
const { user } = require('../../model/mongoMain/mongoConections.js');
const { userValidator } = require('../../validator/userValidator.js');

let validUser;

userRouter.use('/me*', async(req, res, next) => {
    try{
       validUser = await tokenValidation(req.headers.authorization);
       if(! validUser){
            throw new Error(`there is no there is now such user`)
        }
    }
    catch(err){
        return handler.send400Error(res, err.message)
    }

    next();

})

userRouter.get('/me',(req, res) => {
    res.status(200).json({
        user:{
            _id:validUser._id,
            role:validUser.role,
            email:validUser.email,
            created_date:validUser.createdDate
        }
    })
})

userRouter.delete('/me' ,async(req, res) => {
    try{
        if(validateUser.role === `SHIPPER`){
            await user.findByIdAndDelete(validUser._id); 
        }
       else{
           throw new Error(`Driver cant delete his account`)
       }
    }
    catch(err){
        return handler.send400Error(res, err.message)
    }

    res.status(200).json({message:'Profile deleted successfully'})
})

userRouter.patch('/me/password', async(req, res) => {
    try{
        const {oldPassword, newPassword} = req.body;

        if(! (oldPassword && newPassword) ){
            throw new Error('old or new passwords are undefined')
        }
    
        const validationOld = await userValidator(``, oldPassword);
        const validationNew = await userValidator(``, newPassword);
        
        if(! validationOld.value){
           throw new Error(validationOld.message);
        }
        else if(! validationNew.value){
            throw new Error(validationNew.message);
        }
        
        const userFind = await user.findById(validUser._id);
            
        if (! await bcrypt.compare(oldPassword, userFind.password) ){
            throw new Error('passwords didn`t match');
        }
    
        const filter = {_id:validUser._id};
        const update = {password: await bcrypt.hash(newPassword, 10)}
            
        await user.updateOne(filter,update);
    }
    catch(err){
        return handler.send400Error(res, err.message)
    }

    res.status(200).json({message:'Password changed successfully'})
})

module.exports = { userRouter };

