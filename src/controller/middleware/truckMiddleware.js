const express = require('express');
const truckRouter = express.Router();
const { handler } = require('../../utilites/errorHandler.js');
const { isOnLoad } = require('../../utilites/isOnLoad.js');
const { truck } = require('../../model/mongoMain/mongoConections.js');
const { tokenValidation } = require('../../validator/tokenValidation.js');
const { truckTypeValidator } = require('../../validator/truckValidator.js');

let validUser;

truckRouter.use('/*', async(req, res, next) => {
    try{
       validUser = await tokenValidation(req.headers.authorization);

       if(! validUser){
        throw new Error(`there is no such user`)
    }
       
       if(! validUser.role === `DRIVER`){
            throw new Error(`you role are not a driver`);
        }

    }
    catch(err){
        return handler.send400Error(res, err.message);
    }

    next();

})

truckRouter.post(`/`, async(req, res) => {
    const type = req.body.type;

    try{
        await isOnLoad(validUser._id);
        await truckTypeValidator(type);

        const newTruck = new truck({
        created_by:validUser._id,
        type,
        created_date:new Date( Date.now() ),
        })
    
        await newTruck.save();

    }
    catch(err){
        return handler.send400Error(res, err.message);
    }

    res.status(200).json({message:'Truck created successfully'});
})

truckRouter.get(`/`, async(req, res) => {
    let list;
    try{
        list = await truck.find({created_by:validUser._id});
    }
    catch(err){
        return handler.send400Error(res, err.message);
    }

    res.status(200).json({trucks:list});
})

let id;

truckRouter.use(`/:id*`, (req, res, next) => {
    id = req.params.id;
   
    next()
})

truckRouter.get(`/:id`, async(req, res) => {
    let result;
    try{
        result = await truck.findOne({_id:id});
        
        if(! result){
            throw new Error(`there is no truck with this id`)
        }

    }
    catch(err){
        return handler.send400Error(res, err.message);
    }

    res.status(200).json({truck:result});

})

truckRouter.put(`/:id`, async(req, res) => {
    const type = req.body.type;

    try{
        await truckTypeValidator(type)
        await isOnLoad(validUser._id);
        if(! await truck.findOneAndUpdate({_id:id},{type}) ){
            throw new Error(`there is no truck with this id`)
        }
    }
    catch(err){
        return handler.send400Error(res, err.message);
    }

    res.status(200).json({message:`Truck details changed successfully`});
})

truckRouter.delete(`/:id`, async(req, res) => {
    try{
        await isOnLoad(validUser._id);
        
        if(! await truck.findOneAndDelete({_id:id}) ){
            throw new Error(`there is no truck with this id`)
        }
    }
    catch(err){
        return handler.send400Error(res, err.message);
    }

    res.status(200).json({message:`Truck deleted successfully`});
})

truckRouter.post(`/:id/assign`, async(req, res) => {
    let result;
    try{
        await isOnLoad(validUser._id);
        
        if(! await truck.findById({_id:id}) ){
            throw new Error(`there is no truck with such id`)
        }

        result = await truck.find({created_by:validUser._id}); 
        
        if (result.length < 1){
            throw new Error (`You have no trucks`);
        }

        const arr = result.filter((val) => {
            return val.assigned_to == validUser._id
        })

        if(arr.length === 0){
            await truck.findByIdAndUpdate({_id:id}, {assigned_to:validUser._id});
        }
        else{
            await truck.findOneAndUpdate({assigned_to:validUser._id}, {assigned_to:`null`}); 
            await truck.findByIdAndUpdate({_id:id}, {assigned_to:validUser._id});
        }

    }
    catch(err){
        return handler.send400Error(res, err.message);
    }

    res.status(200).json({message:`Truck assigned successfully`});
})



module.exports = { truckRouter };