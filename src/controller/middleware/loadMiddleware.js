const express = require('express');
const loadRouter = express.Router();
const { handler } = require('../../utilites/errorHandler.js');
const { load,truck } = require('../../model/mongoMain/mongoConections.js');
const { tokenValidation } = require('../../validator/tokenValidation.js');
const { roleChecker } = require('../../utilites/roleChecker.js');
const { changeState } = require('../../utilites/changeState.js');
const { truckModels } = require('../../utilites/truckModels.js');

let validUser;

loadRouter.use('/*', async(req, res, next) => {
    try{
       validUser = await tokenValidation(req.headers.authorization);

       if(! validUser){
        throw new Error(`there is no such user`)
        }
    }

    catch(err){
        return handler.send400Error(res, err.message);
    }

    next();

})
loadRouter.post(`/`,async(req, res) => {    
    try{
        await roleChecker(`SHIPPER`,validUser.role)
        const {
            name,
            payload,
            pickup_address,
            delivery_address,
            dimensions:{
                width,
                length,
                height
            }
        } = req.body;
        
        const newLoad = new load({
            created_by:validUser._id,
            assigned_to:validUser._id,
            name,
            payload,
            pickup_address,
            delivery_address,
            dimensions:{
                width,
                length,
                height
            },

            created_date:new Date( Date.now() ),
            })
        
            await newLoad.save();
    }
    catch(err){
        return handler.send400Error(res, err.message);
    }

    res.status(200).json({message:'Load created successfully'});
})

loadRouter.get(`/`, async(req, res) => {
    let status = req.query.status;
    let limit = req.query.limit;
    let offset = req.query.offset;
    let result;

    try{
        if(! limit ){
            limit = 10;
        }
        else if(limit <= 0 && limit > 50){
            limit = 50;
        }

        if(! offset){
            offset = 0;
        }

        const options = {
            page: 1,
            limit,
            offset,
        };
        
        if(validUser.role == `SHIPPER`){
            if(! status){
                result = await load.paginate({assigned_to:validUser._id}, options);
            }
            else if(
                status == `NEW` || 
                status == `POSTED` || 
                status == `ASSIGNED` ||
                status == `SHIPPED` 
            ){
                result = await load.paginate({status, assigned_to:validUser._id}, options);
                console.log(status)
            }
            else{
                throw new Error(`wrong status`);
            }
        }
        else if(validUser.role == `DRIVER`){
            if(! status){
                status = `SHIPPED`;
            }
            if(
                status == `ASSIGNED` || 
                status == `SHIPPED` 
            ){
                result = await load.paginate({
                    status,
                    "logs.message":`Load assigned to driver with id ${validUser._id}`
                }, options);
            }
            else{
                throw new Error(`wrong status`);
            }
        }
    }
    catch(err){
         return handler.send400Error(res, err.message);
    }

    res.status(200).json({loads:result.docs});
})

loadRouter.get(`/active`, async(req, res) => {
    let result;
    try{
        await roleChecker(`DRIVER`,validUser.role);

        result = await load.findOne({
          status:`ASSIGNED`,
          "logs.message":`Load assigned to driver with id ${validUser._id}`
        });
      
        if(! result){
            return handler.send500Error(res, `something went wrong`); 
        }
    
    }
    catch(err){
        return handler.send400Error(res, err.message);  
    }

    res.status(200).json({load:result});
})

loadRouter.patch(`/active/state`, async(req, res) => {
    let result;
    let str;
    try{
        await roleChecker(`DRIVER`,validUser.role);

        result = await load.findOne({
            status:`ASSIGNED`,
            "logs.message":`Load assigned to driver with id ${validUser._id}`
          });
        
          if(! result){
              return handler.send500Error(res, `something went wrong`); 
          }

          str = await changeState(result.state);
          if(str === `Arrived to delivery`){
            await load.findByIdAndUpdate({_id:result._id},{state:str,status:'SHIPPED'});
            await truck.findByIdAndUpdate({_id:validUser._id,status:'OL'},{status:'IS'});
          }
          else{
            await load.findByIdAndUpdate({_id:result._id},{state:str});
          }
      
    }
    catch(err){
        return handler.send400Error(res, err.message);  
    }

    res.status(200).json({"message": `Load state changed to '${str}'`});
})

loadRouter.get(`/:id`, async(req, res) => {
    const _id = req.params.id;
    let result;
    try{
        result = await load.findOne({_id});
            
        if(! result){
            throw new Error(`there is no load with this id`)
        }
    
    }
    catch(err){
        return handler.send400Error(res, err.message);
    }
    
    res.status(200).json({truck:result});
    
})

loadRouter.delete(`/:id`, async(req, res) => {
    const _id = req.params.id;
    try{
        await roleChecker(`SHIPPER`,validUser.role)
        
        if(! await load.findOneAndDelete({_id}) ){
            throw new Error(`there is no load with this id`)
        }
    }
    catch(err){
        return handler.send400Error(res, err.message);
    }

    res.status(200).json({message:`Load deleted successfully`});
})

loadRouter.put(`/:id`, async(req, res) => {
    const _id = req.params.id;
    const {
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions:{
            width,
            length,
            height
        }
    } = req.body;

    const obj = {
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions:{
            width,
            length,
            height
        }
    }

    try{
        await roleChecker(`SHIPPER`,validUser.role)
        
        if(! await load.findOneAndUpdate({_id}, obj)){
            throw new Error(`there is no load with this id`)
        }
    }
    catch(err){
        return handler.send400Error(res, err.message);
    }

    res.status(200).json({message:`Load details changed successfully`});
})

loadRouter.post(`/:id/post`, async(req, res) => {
    const _id = req.params.id;
    let theLoad;
    let theTruck;
    let theTruckParams;
    try{
        function checkDim(arr){
            if(! Array.isArray(arr) && (! arr.length === 0)){
                return false
            }

            for(let i = 0; i < arr.length; i++){
                const truckEX = truckModels[arr[i].type];
                if(
                    truckEX.dimensions.width > theLoad.dimensions.width &&
                    truckEX.dimensions.length > theLoad.dimensions.length &&
                    truckEX.dimensions.height > theLoad.dimensions.height &&
                    truckEX.payload > theLoad.payload   
                ){  
                    return( 
                        {
                            driver: arr[i].assigned_to,
                            id: arr[i]._id,
                        }
                    ) 
                }
                else{
                    return false
                }
            }
            
        }
        
        await roleChecker(`SHIPPER`,validUser.role)

        theLoad = await load.findOne({_id});
        theTruck = await truck.find({status:'IS', assigned_to:{$ne:`null`}});
        theTruckParams = checkDim(theTruck);
        
        if( (! theTruck) || (! checkDim(theTruck)) ){
            return res.status(200).json({"message": "Load not posted","driver_found": false})
        }
        
        if(! theLoad || ( theLoad.status != 'NEW') ){
            throw new Error(`there is no load with this id or this load shipping to you `)
        }
        if(
            ! await truck.findByIdAndUpdate({_id:theTruckParams.id}, {status:'OL'}) ||
            ! await load.findByIdAndUpdate(
               {_id}, 
                {
                    status:'ASSIGNED', 
                    state:'En route to Pick Up',
                    $push:{
                        logs:{
                        'message':`Load assigned to driver with id ${theTruckParams.driver}`,
                        'time': new Date( Date.now() ),
                        }
                    }   
                }
            )
        ){
            return handler.send500Error(res, `something went wrong`);
        } 

    }

    catch(err){
        return handler.send400Error(res, err.message);
    }
    
    res.status(200).json({"message": "Load posted successfully","driver_found": true});
    
})



module.exports = { loadRouter };