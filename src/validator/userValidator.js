const joi = require('joi');
const { userValidatorSchema } = require('./validateSchema.js');

const userValidator = async (email, password, role) => {
    try{
        await userValidatorSchema.validateAsync({email, password, role});
        return {
            value:true,
            message:`validation passed!`}
    }
    catch(err){
        return {
            value:false,
            message:err
        }
    }
}

module.exports = { userValidator };