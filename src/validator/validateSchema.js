const joi = require('joi');

const userValidatorSchema = joi.object().keys({
    email: joi.string()
        .allow('')
        .email(),
    // уникальность и обязательность проверяется в схеме пользователя
    password: joi.string()
        .allow('')
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
    
    role: joi.string()
        .valid(`SHIPPER`, `DRIVER`)
}) 

    module.exports = { userValidatorSchema };