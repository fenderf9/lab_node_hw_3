const jwt = require('jsonwebtoken');
require('dotenv').config();
const { user } = require('../model/mongoMain/mongoConections.js');

const tokenValidation = async (token) => {
    
    const {email, role, _id} = jwt.verify(token.split(` `)[1], process.env.SECRET);

        if (! (email && role) ){
            throw new Error('email or role are not defined/from:(jwt_val)');
        }

        const findUser = await user.findOne({email, role, _id});

        if (! findUser){
            throw new Error('user not found/from:(jwt_val)')
        }

        return findUser
    
}

module.exports = { tokenValidation };