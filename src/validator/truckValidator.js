const joi = require('joi');
const { truckValidatorSchema } = require('./truckTypeValidationSchema.js');

const truckTypeValidator = async (type) => {
        await truckValidatorSchema.validateAsync({type});
}

module.exports = { truckTypeValidator };