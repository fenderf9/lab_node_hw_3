const joi = require('joi');

const truckValidatorSchema = joi.object().keys({
    
    type: joi.string()
        .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
}) 

    module.exports = { truckValidatorSchema };